Process previously failed backfill events in the background to avoid blocking requests for something that is bound to fail again.
